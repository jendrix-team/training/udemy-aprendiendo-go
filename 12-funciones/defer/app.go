package main

import "fmt"

func main() {
	fmt.Println("Go App - DEFER!")
	// esta funcion se va a ejecutar cuando la funcion que la contiene
	// finaliza su ejecucion.
	defer foo()
	bar()
}

func foo() {
	fmt.Println("foo")
}

func bar() {
	fmt.Println("bar")
}
