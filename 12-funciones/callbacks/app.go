package main

import "fmt"

func main() {
	fmt.Println("Go App - Callbacks")

	numeros := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println("Numeros: ", numeros)

	multiplo := 11
	multiplos := filtrarMultiplos(multiplo, numeros...)
	fmt.Printf("Multiplos de %d : %v\n", multiplo, multiplos)

	result := aplicarFuncion(suma, multiplos...)
	fmt.Println("resultado: ", result)
}

func suma(numbers ...int) int {
	sum := 0
	for _, v := range numbers {
		sum += v
	}
	return sum
}

func filtrarMultiplos(multiplo int, numbers ...int) []int {
	pares := []int{}
	for _, v := range numbers {
		if v%multiplo == 0 {
			pares = append(pares, v)
		}
	}
	return pares
}

func aplicarFuncion(fx func(...int) int, numbers ...int) int {
	return fx(numbers...)
}
