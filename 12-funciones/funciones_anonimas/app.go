package main

import "fmt"

func main() {
	fmt.Println("Go App - Funciones Anonimas!")

	func(x int) {
		fmt.Println("La funcion anonima se ejecuto con el numero", x)
	}(12)

	fx := func(numbers ...int) int {
		sum := 0
		for _, v := range numbers {
			sum += v
		}
		return sum
	}

	result := fx(1, 5)
	fmt.Println("El resultado es: ", result)
}
