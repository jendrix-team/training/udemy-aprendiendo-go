package main

import "fmt"

type persona struct {
	nombre   string
	apellido string
	edad     int
}

type jugador struct {
	persona
	posicion string
	camiseta int
}

func (p persona) esMayorDeEdad() bool {
	return p.edad >= 18
}

func (j jugador) esArquero() bool {
	return j.posicion == "ARQUERO"
}

func (j jugador) esDefensor() bool {
	return j.posicion == "DEFENSOR"
}

func main() {
	fmt.Println("Go App - Metodos")
	bacchia := jugador{
		persona: persona{
			nombre:   "Agustin",
			apellido: "Bacchia",
			edad:     17,
		},
		posicion: "ARQUERO",
		camiseta: 12,
	}

	fmt.Println("Jugador: ", bacchia.apellido, ", esArquero?: ", bacchia.esArquero(), ", esMayorDeEdad:", bacchia.esMayorDeEdad())
}
