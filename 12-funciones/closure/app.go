package main

import "fmt"

func main() {
	fmt.Println("Go App - Closure")

	fx1 := incrementor()
	fx2 := incrementor()

	fmt.Println(fx1())
	fmt.Println(fx1())
	fmt.Println(fx1())

	fmt.Println(fx2())
	fmt.Println(fx2())
	fmt.Println(fx2())

}

func incrementor() func() int {
	var x int
	return func() int {
		x++
		return x
	}
}
