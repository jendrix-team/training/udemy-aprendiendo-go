package main

import "fmt"

type Persona struct {
	nombre   string
	apellido string
	edad     int
}

type Humano interface {
	esMayorDeEdad() bool
	presentarse()
}

type Jugador struct {
	Persona
	posicion string
	camiseta int
}

func (p Persona) esMayorDeEdad() bool {
	return p.edad >= 18
}

func (p Persona) presentarse() {
	fmt.Println("Mi nombre es: ", p.nombre, p.apellido, " y soy una persona")
}

func (j Jugador) presentarse() {
	fmt.Println("Mi nombre es: ", j.nombre, j.apellido, ", soy un jugador de FUTBOL y mi posicion es ", j.posicion)
}

func (j Jugador) esArquero() bool {
	return j.posicion == "ARQUERO"
}

func (j Jugador) esDefensor() bool {
	return j.posicion == "DEFENSOR"
}

func saludar(h Humano) {
	fmt.Println("Saludando al humano:", h)
	switch h.(type) {
	case Persona:
		fmt.Println(h.(Persona).nombre, "es una persona")
	case Jugador:
		fmt.Println(h.(Jugador).nombre, "es un jugador")
	}
}

func main() {
	fmt.Println("Go App - Metodos")
	bacchia := Jugador{
		Persona: Persona{
			nombre:   "Renzo",
			apellido: "Bacchia",
			edad:     17,
		},
		posicion: "ARQUERO",
		camiseta: 12,
	}

	bacchia.Persona.presentarse()
	bacchia.presentarse()

	agus := Persona{
		nombre:   "Agustina",
		apellido: "Fernandez",
		edad:     7,
	}
	agus.presentarse()

	saludar(bacchia)
	saludar(agus)
}
