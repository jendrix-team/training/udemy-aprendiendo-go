package main

import "fmt"

func main() {
	fmt.Println("Go App - FUNCIONES")
	fx := operacion("*")
	fmt.Printf("%T\n", fx)
	result := fx(5, 3)
	fmt.Println("El resultado es ", result)
}

func operacion(operador string) func(...int) int {
	if operador == "+" {
		return suma
	} else {
		return multiplicacion
	}
}

func suma(numbers ...int) int {
	sum := 0
	for _, v := range numbers {
		sum += v
	}
	return sum
}

func multiplicacion(numbers ...int) int {
	sum := 1
	for _, v := range numbers {
		sum *= v
	}
	return sum
}
