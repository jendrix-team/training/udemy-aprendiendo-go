package main

import (
	"fmt"
	"math"
)

type Cuadrado struct {
	Longitud float64
}

func (c Cuadrado) Area() float64 {
	return math.Pow(c.Longitud, 2)
}

type Circulo struct {
	Radio float64
}

func (c Circulo) Area() float64 {
	return math.Pi * math.Pow(c.Radio, 2)
}

type Figura interface {
	Area() float64
}

func main() {
	fmt.Println("Go App - Intefaces")
	cuadrado := Cuadrado{
		Longitud: 2.5,
	}

	circulo := Circulo{
		Radio: 4.5,
	}

	info(cuadrado)
	info(circulo)

}

func info(f Figura) {
	fmt.Printf("Figura: %T, Area: %f\n", f, f.Area())
}
