package main

import "fmt"

func main() {
	fmt.Println("Go App - Recursividad")
	factorialNumero := 4
	result := factorial(factorialNumero)
	fmt.Printf("\nEl factorial de %d es %d", factorialNumero, result)
}

func factorial(n int) int {
	if n > 1 {
		fmt.Print(n, " * ")
		return n * factorial(n-1)
	} else {
		fmt.Print(1)
		return 1
	}
}
