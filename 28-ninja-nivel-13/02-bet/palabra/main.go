//Package palabra no ayuda a trabaja con palabras
package palabra

import "strings"

//ConteoUso no ayuda un monton. mmmmmm....
func ConteoUso(s string) map[string]int {
	xs := strings.Fields(s)
	m := make(map[string]int)
	for _, v := range xs {
		m[v]++
	}
	return m
}

//Conteo es una gran funcion que devuelve la cantidad de palabras
func Conteo(s string) int {
	return len(strings.Fields(s))
}
