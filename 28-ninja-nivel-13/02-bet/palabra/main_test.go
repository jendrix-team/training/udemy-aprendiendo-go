package palabra

import (
	"fmt"
	"jendrix/training/udemy/28-ninja-nivel-13/02-bet/cita"
	"testing"
)

func TestConteo(t *testing.T) {
	v := Conteo("My name is Willy")
	if v != 4 {
		t.Error("Expect:", 4, "Got:", v)
	}
}

func BenchmarkConteo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Conteo(cita.Cuando)
	}
}

func ExampleConteo() {
	v := Conteo("My name is Willy")
	fmt.Println(v)
	//Output: 4
}

func TestConteoUso(t *testing.T) {
	m := ConteoUso("dos por dos")
	if len(m) != 2 {
		t.Error("Expected: ", 2, "Got:", len(m))
	} else {
		if m["dos"] != 2 {
			t.Error("Expected (dos): ", 2, "Got:", m["dos"])
		} else if m["por"] != 1 {
			t.Error("Expected (por): ", 2, "Got:", m["por"])
		}
	}
}

func TestConteUso1(t *testing.T) {
	m := ConteoUso("uno dos tres tres tres")
	for k, v := range m {
		switch k {
		case "uno":
			if v != 1 {
				t.Error("Esperaba", 1, "Obtuvo", v)
			}
		case "dos":
			if v != 1 {
				t.Error("Esperaba", 1, "Obtuvo", v)
			}
		case "tres":
			if v != 3 {
				t.Error("Esperaba", 3, "Obtuvo", v)
			}
		}
	}
}

func BenchmarkConteoUso(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConteoUso(cita.Cuando)
	}
}

func ExampleConteoUso() {
	for k, v := range ConteoUso("dos por dos es cuatro") {
		fmt.Println(v, k)
	}
	//Output:
	// 2 dos
	// 1 por
	// 1 es
	// 1 cuatro
}
