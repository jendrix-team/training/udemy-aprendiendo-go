package main

import (
	"fmt"
	"jendrix/training/udemy/28-ninja-nivel-13/02-bet/cita"
	"jendrix/training/udemy/28-ninja-nivel-13/02-bet/palabra"
)

func main() {
	fmt.Println(palabra.Conteo(cita.Cuando))

	for k, v := range palabra.ConteoUso(cita.Cuando) {
		fmt.Println(v, k)
	}
}
