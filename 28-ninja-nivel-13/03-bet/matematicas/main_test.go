package matematicas

import (
	"fmt"
	"testing"
)

type prueba struct {
	datos     []int
	resultado float64
}

func TestPromedioCentrado(t *testing.T) {
	table := []prueba{
		prueba{datos: []int{2, 4, 6, 8, 10}, resultado: 6},
		prueba{datos: []int{1, 3, 5, 7, 9}, resultado: 5},
	}
	for _, v := range table {
		r := PromedioCentrado(v.datos)
		if r != v.resultado {
			t.Error("Expect:", v.resultado, "Got", r)
		}
	}
}

func ExamplePromedioCentrado() {
	v := PromedioCentrado([]int{2, 4, 6, 8, 10})
	fmt.Println(v)
	//Output: 6
}

func BenchmarkPromedioCentrado(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PromedioCentrado([]int{2, 4, 6, 8, 10})
	}
}
