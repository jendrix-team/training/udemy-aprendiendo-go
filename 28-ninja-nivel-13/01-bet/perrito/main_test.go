package perrito

import (
	"fmt"
	"testing"
)

func TestEdad(t *testing.T) {
	v := Edad(2)
	if v != 14 {
		t.Errorf("Expect: %d, Got: %d", 14, v)
	}
}

func ExampleEdad() {
	v := Edad(2)
	fmt.Println(v)
	//Output: 14
}

func BenchmarkEdad(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Edad(3)
	}
}

func TestEdadDos(t *testing.T) {
	v := EdadDos(2)
	if v != 14 {
		t.Errorf("Expect: %d, Got: %d", 14, v)
	}
}

func ExampleEdadDos() {
	v := EdadDos(2)
	fmt.Println(v)
	//Output: 14
}

func BenchmarkEdadDos(b *testing.B) {
	for i := 0; i < b.N; i++ {
		EdadDos(3)
	}
}
