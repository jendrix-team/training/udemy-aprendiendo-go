package main

import "testing"

func TestMisuma(t *testing.T) {

	v := miSuma(2, 4)
	if v != 6 {
		t.Errorf("Expect: %d, Got: %d", 10, v)
	}
}
