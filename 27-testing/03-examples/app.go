package main

import (
	"fmt"
	"jendrix/training/udemy/27-testing/examples/mate"
)

func main() {
	fmt.Println("Go App - Examples")

	fmt.Println(mate.Sum(1, 3, 4))
	fmt.Println(mate.Sum(2, 7, 1))
}
