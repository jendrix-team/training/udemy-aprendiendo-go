//Package mate nos ayuda a comprobar tus habilidades con las sumas.
package mate

//Sum realiza la suma de los numeros enteros pasados parametros.
func Sum(numbers ...int) int {
	result := 0
	for _, n := range numbers {
		result += n
	}
	return result
}
