package mate

import "fmt"

func ExampleSum() {
	r := Sum(2, 3, 5)
	fmt.Println(r)
	//Output:
	//10
}
