package talk

import (
	"fmt"
	"testing"
)

func TestWelcolme(t *testing.T) {
	s := Welcome("Agus")
	if s != "Welcome Agus!" {
		t.Error("Expected", "Welcome Agus!", "Got", s)
	}
}

func ExampleWelcome() {
	fmt.Println(Welcome("Agus"))
	//Output: Welcome Agus!
}

func BenchmarkWelcome(b *testing.B) {

	for i := 0; i < b.N; i++ {
		//Welcome("Agus " + string(i)) 210ns
		Welcome("Agus")
	}
}
