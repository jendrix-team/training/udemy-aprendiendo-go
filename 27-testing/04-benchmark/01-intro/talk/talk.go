//Package talk nos permite realizar interactuar con el usuario
package talk

import "fmt"

// Welcome realiza un saludo de bienvenida a una persona
func Welcome(s string) string {
	return fmt.Sprintf("Welcome %s!", s)
}
