package main

import "fmt"

func main() {
	fmt.Println("Go App - Punteros")

	a := 1978
	fmt.Println("valor (a): ", a)
	fmt.Println("direccion memoria (&a): ", &a)
	fmt.Printf("tipo (a): %T\n", a)
	fmt.Printf("tipo (&a): %T\n", &a)

	var b *int = &a
	fmt.Println("valor (b): ", b)
	fmt.Println("direccion memoria (&b): ", &b)
	fmt.Println("Contenido (*b): ", *b)

	*b = *b + 41
	fmt.Println("valor (a): ", a)
}
