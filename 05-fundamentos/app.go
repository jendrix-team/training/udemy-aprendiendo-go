package main

import "fmt"

import "github.com/google/uuid"

func main() {
	fmt.Println("Fundamentos")
	uuid := uuid.New()
	fmt.Println(uuid.String())

	m := make(map[string]int)
	fmt.Println(m)
	m["k1"] = 23
	m["k2"] = 45
	fmt.Println(m)
	m["k1"] = 11
	fmt.Println(m)

	m1 := map[string]int{
		"k1": 1,
		"k2": 2,
		"k3": 4,
	}
	fmt.Println(m1)

	delete(m1, "k2")
	fmt.Println(m1)

}
