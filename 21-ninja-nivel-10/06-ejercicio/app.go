package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup

	c := make(chan int)

	wg.Add(1)
	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
		wg.Done()
		close(c)
	}()

	go func() {
		for v := range c {
			fmt.Println(v)
		}
		fmt.Println("Finalizando APP")
	}()

	wg.Wait()
}
