package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup

	c := make(chan string)

	gorutines := 4
	wg.Add(gorutines)
	for j := 1; j <= gorutines; j++ {
		go func(v int) {
			for i := 1; i <= 5; i++ {
				c <- fmt.Sprintf("j: %d, i: %d ", v, i)
			}
			wg.Done()
		}(j)
	}

	go func() {
		for v := range c {
			fmt.Println(v)
		}
		fmt.Println("Finalizando APP")
	}()

	wg.Wait()
	fmt.Println("Terminaron de ejecutarse todas las gorutines")
	close(c)
}
