package main

import (
	"fmt"
)

func main() {
	// canal sin buffer
	c := make(chan int)

	go func() {
		c <- 42
	}()

	fmt.Println(<-c)

	// canal con buffer
	ch := make(chan int, 1)
	ch <- 78
	fmt.Println(<-ch)

}
