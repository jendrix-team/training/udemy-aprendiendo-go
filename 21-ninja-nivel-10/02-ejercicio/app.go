package main

import (
	"fmt"
)

func main() {

	cs := make(chan int)

	go func(c chan<- int) {
		c <- 42
		fmt.Printf("c\t%T\n", c)
	}(cs)

	fmt.Println(<-cs)

	fmt.Printf("------\n")
	fmt.Printf("cs\t%T\n", cs)
}
