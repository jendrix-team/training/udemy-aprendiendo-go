package main

import (
	"encoding/json"
	"fmt"
)

type Persona struct {
	Nombre   string `json:"Nombre"`
	Apellido string `json:"Apellido"`
	Edad     int    `json:"Edad"`
}

func main() {
	fmt.Println("Go App - JSON")
	marshallJSON()
	unmarshallJSON()
}

func marshallJSON() {

	agus := Persona{
		Nombre:   "Agustina",
		Apellido: "Fernandez Girones",
		Edad:     7,
	}

	joaquin := Persona{
		Nombre:   "Joaquin",
		Apellido: "Fernandez Girones",
		Edad:     4,
	}

	personas := []Persona{agus, joaquin}
	fmt.Println(personas)

	bytes, err := json.Marshal(personas)
	if err != nil {
		fmt.Println("Error in marshalling", err)
	}
	fmt.Printf("%+v\n", string(bytes))

}

func unmarshallJSON() {
	data := `[
		{
		"Nombre":"Agustina",
		"Apellido":"Fernandez Girones",
		"Edad":7},
		{
			"Nombre":"Joaquin",
			"Apellido":"Fernandez Girones",
			"Edad":4}
			]`
	fmt.Println(data)
	var personas []Persona
	err := json.Unmarshal([]byte(data), &personas)
	if err != nil {
		fmt.Println("Error in unmarshalling", err)
	}
	fmt.Println(personas)
}
