package main

import "fmt"
import "golang.org/x/crypto/bcrypt"

func main() {
	fmt.Println("Go App - Bcrypt")
	password := "willy"
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 4)
	if err != nil {
		fmt.Println("Error encriptando el password")
		return
	}
	fmt.Println(string(bytes))
}
