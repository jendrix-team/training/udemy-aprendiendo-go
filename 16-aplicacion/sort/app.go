package main

import (
	"fmt"
	"sort"
)

type Persona struct {
	Nombre string
	Edad   int
}

type SortByEdad []Persona
type SortByNombre []Persona

func (p SortByEdad) Len() int {
	return len(p)
}

func (p SortByEdad) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p SortByEdad) Less(i, j int) bool {
	return p[i].Edad < p[j].Edad
}

func (p SortByNombre) Len() int {
	return len(p)
}

func (p SortByNombre) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p SortByNombre) Less(i, j int) bool {
	return p[i].Nombre < p[j].Nombre
}

func main() {
	fmt.Println("Go App - Sort")

	numbers := []int{4, 7, 3, 42, 99, 18, 16, 56, 12}
	names := []string{"James", "Q", "M", "Moneypenny", "Dr. No"}

	sort.Ints(numbers)
	fmt.Println(numbers)
	sort.Strings(names)
	fmt.Println(names)

	p1 := Persona{"Eduar", 32}
	p2 := Persona{"María", 25}
	p3 := Persona{"Carolina", 56}
	p4 := Persona{"Adriana", 60}

	personas := []Persona{p1, p2, p3, p4}

	sort.Sort(SortByEdad(personas))
	fmt.Println(personas)

	sort.Sort(SortByNombre(personas))
	fmt.Println(personas)

}
