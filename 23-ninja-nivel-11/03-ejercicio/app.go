package main

import (
	"fmt"
)

type errorPer struct {
	message string
}

func (ep errorPer) Error() string {
	return fmt.Sprintf("Error: %s", ep.message)
}

func main() {
	e1 := errorPer{message: "Este es un error personalizado"}
	foo(e1)
}

func foo(e error) {
	fmt.Println(e.Error())
}
