package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	fmt.Println("Go App - Atomic")

	var sequenceNumber int64
	gorutines := 100

	var waitGroup sync.WaitGroup
	waitGroup.Add(gorutines)

	for i := 0; i < gorutines; i++ {
		go func() {
			atomic.AddInt64(&sequenceNumber, 1)
			fmt.Println("SequenceNumber: ", atomic.LoadInt64(&sequenceNumber))
			waitGroup.Done()
		}()
	}
	waitGroup.Wait()
	fmt.Println("FIN sequence value: ", sequenceNumber)
	fmt.Println("App Finalizada")
}
