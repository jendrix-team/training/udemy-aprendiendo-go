package main

import "fmt"

type Persona struct {
	Nombre string
	Edad   int
}

func (p *Persona) Saludar(d Persona) {
	fmt.Println(p.Nombre + " esta saludando a " + d.Nombre)
}

type Humano interface {
	Saludar(Persona)
}

func main() {
	fmt.Println("Go App - Method Sets")

	agus := Persona{
		Nombre: "Agustina",
		Edad:   7,
	}

	joquin := Persona{
		Nombre: "Joaquin",
		Edad:   4,
	}

	presentarse(&agus, joquin)
}

func presentarse(h1 Humano, d Persona) {
	h1.Saludar(d)
}
