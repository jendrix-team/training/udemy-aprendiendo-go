package main

import (
	"fmt"
	"sync"
)

func main() {
	fmt.Println("Go App - Mutex")

	sequenceNumber := 0
	gorutines := 10

	var mutex sync.Mutex
	var waitGroup sync.WaitGroup

	waitGroup.Add(gorutines)

	for i := 0; i < gorutines; i++ {
		go func() {
			mutex.Lock()
			c := sequenceNumber
			fmt.Println("SequenceNumber: ", sequenceNumber)
			c++
			sequenceNumber = c
			mutex.Unlock()
			waitGroup.Done()
		}()
	}
	waitGroup.Wait()
	fmt.Println("SequenceNumber: ", sequenceNumber)
	fmt.Println("App Finalizada")
}
