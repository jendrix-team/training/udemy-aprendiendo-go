package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Go App - Race Condition")
	var waitGroup sync.WaitGroup
	sequenceNumber := 0
	gorutines := 10

	waitGroup.Add(gorutines)
	for i := 0; i < gorutines; i++ {
		go func() {
			c := sequenceNumber
			runtime.Gosched()
			fmt.Println("SequenceNumber: ", sequenceNumber)
			c++
			sequenceNumber = c
			waitGroup.Done()
		}()
	}
	waitGroup.Wait()
	fmt.Println("SequenceNumber: ", sequenceNumber)
	fmt.Println("Race Condition Finalizada")
}
