package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("Go App - OS & ARCH")
	fmt.Println("OS: ", runtime.GOOS)
	fmt.Println("ARCH: ", runtime.GOARCH)
}
