package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("Go App - gorutines & concurrencia")

	fmt.Printf("CPUs: %d\n", runtime.NumCPU())
	fmt.Printf("Nro Gorutines: %d\n", runtime.NumGoroutine())
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		fmt.Println("Saludando desde la primer gorutine")
		wg.Done()
	}()

	go func() {
		fmt.Println("Saludando desde la segunda gorutine")
		wg.Done()
	}()

	fmt.Printf("Nro Gorutines: %d\n", runtime.NumGoroutine())
	wg.Wait()
	fmt.Println("Gorutines finalizadas!!!")
}
