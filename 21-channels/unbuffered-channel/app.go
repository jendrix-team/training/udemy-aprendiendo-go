package main

import "fmt"

func main() {
	fmt.Println("Go App - Unbuffered Channel")
	// tienen que existir 2 gourutines para que el canal no bloquee la app
	// una gourutine emite y la otra recibe un mensaje por el canal, si no hay
	// una gorutine que reciba el mensaje la app se bloquea
	channel := make(chan int)

	go func() {
		channel <- 78
	}()

	fmt.Println("Recibiendo mensaje de la gorutine anonima: ", <-channel)
}
