package main

import (
	"fmt"
)

func main() {
	fmt.Println("Go App - Canales direccionales")
	// var waitGroup sync.WaitGroup

	ch := make(chan int)
	fmt.Println("%T", ch)

	go enviar(ch)

	recibir(ch)

	fmt.Println("Finalizando App")
}

func enviar(channel chan<- int) {
	channel <- 78
}

func recibir(channel <-chan int) {
	valor := <-channel
	fmt.Println("valor: ", valor)
}
