package main

import "fmt"

func main() {
	fmt.Println("Go App - Select")
	channelPar := make(chan int)
	channelImpar := make(chan int)
	channelSalir := make(chan int)
	defer closeChannels(channelPar, channelImpar, channelSalir)

	go enviar(channelPar, channelImpar, channelSalir)

	recibir(channelPar, channelImpar, channelSalir)

	fmt.Println("App Finalizada")
}

func closeChannels(cp, ci, cs chan int) {
	fmt.Println("Cerrando canales")
	close(cp)
	close(ci)
	close(cs)
}

//func enviar(cp chan<- int, ci chan<- int, cs chan<- int) {
func enviar(cp, ci, cs chan<- int) {
	for i := 1; i < 10; i++ {
		if i%2 == 0 {
			cp <- i
		} else {
			ci <- i
		}
	}
	cs <- 0
	//close(cp)
	//close(ci)
	//close(cs)
}

//func recibir(cp <-chan int, ci <-chan int, cs <-chan int) {
func recibir(cp, ci, cs <-chan int) {
	for {
		select {
		case v := <-cp:
			fmt.Println("Channel par: ", v)
		case v := <-ci:
			fmt.Println("Channel Impar: ", v)
		case <-cs:
			fmt.Println("Finalizar transmicion")
			return
		}
	}
}
