package main

import "fmt"

func main() {
	fmt.Println("Go App - Range")
	ch := make(chan int)

	go func() {
		for i := 0; i < 5; i++ {
			ch <- i
		}
		close(ch)
	}()

	recibir(ch)

	fmt.Println("App finalizada")
}

func recibir(channel <-chan int) {
	for v := range channel {
		fmt.Println("valor recibido: ", v)
	}
}
