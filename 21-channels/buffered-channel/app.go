package main

import "fmt"

func main() {
	fmt.Println("Go App - Buffered Channel")
	channel := make(chan int, 2)

	channel <- 78
	channel <- 74

	fmt.Println("Recibiendo el primer mensaja del channel: ", <-channel)
	fmt.Println("Recibiendo el segundo mensaja del channel: ", <-channel)
}
