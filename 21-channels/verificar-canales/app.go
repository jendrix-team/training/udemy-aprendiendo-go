package main

import "fmt"

func main() {
	fmt.Println("Go App - Verificar Canales Abiertos/Cerrados")
	ch := make(chan int)

	go func() {
		ch <- 42
		close(ch)
	}()

	valor, ok := <-ch
	fmt.Println(valor, ok)
	valor, ok = <-ch
	fmt.Println(valor, ok)

	fmt.Println("App Finalizada")
}
